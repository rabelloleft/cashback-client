import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ListaComponent } from './empresa/lista/lista.component';
import { EmpresaComponent } from './empresa/empresa/empresa.component';

const routes: Routes = [
  { path : "home", component: HomeComponent },
  
  { path : "empresa", component: ListaComponent },
  { path : "empresa/:id", component : EmpresaComponent },
  { path : "empresa/novo", component : EmpresaComponent },

  { path : "login", component: LoginComponent },
  { path: '', pathMatch: 'full', redirectTo: '/login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
