import { Injectable, OnInit } from '@angular/core';
import { LoginReturn } from './entity/login';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private item : string = "cashback";
  private id : Number;
  private token : String;
  private papeis : String[];

  constructor() { 
    let auth : any = sessionStorage.getItem( this.item );
    if ( auth != null ) {
      auth = JSON.parse(auth);
      this.id = auth.id;
      this.token = auth.token;      
      this.papeis = auth.papeis;
    }
  }

  read() : any {
    let auth: any = sessionStorage.getItem( this.item );
    return auth;
  }

  login( login : LoginReturn ) {
    this.id = login.id;
    this.token = login.token;    
    this.papeis = login.papeis;
    sessionStorage.setItem( this.item, 
      JSON.stringify( {
            id : this.id,    
         token : this.token,        
        papeis : this.papeis
      } )
    );
  }

  loggout() {
    sessionStorage.removeItem( this.item );
  }

  isValidSeasson() : boolean {
    let auth: any = this.read();
    if (auth != null) {    
      auth = JSON.parse( auth );
      return ( auth.token != null && auth.papeis.length > 0 );
    }
    return false;
  }

  getId() {
    return this.id;
  }

  getToken() {
    return this.token;
  }

  hasRole( role: string ) {
    return this.papeis.indexOf( role ) != -1;
  }

}