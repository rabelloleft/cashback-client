import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor( private a: AuthService ) { }

  intercept(req : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {
    let token = this.a.getToken();
    if ( token != null ) { return next.handle( req.clone( { headers : req.headers.set("Authorization", "Bearer " + token) } ) ); } 
    else { return next.handle( req ); }
  }

}