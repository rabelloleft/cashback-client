import { Component, OnInit } from '@angular/core';
import { LoginEntry, LoginReturn } from '../entity/login';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { environment } from '../../environments/environment';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  erro : boolean;
  le: LoginEntry = new LoginEntry();
  WS = `${environment.api}/login/`;
  msgs: Message[] = [];

  constructor( private a: AuthService, 
               private h: HttpClient,
               private r: Router ) { }

  ngOnInit() {
    if ( this.a.isValidSeasson() ) {
      this.toHome();
    }
    this.erro = false; 
  }

  entrar() {
    this.h.post<LoginReturn>( this.WS, this.le ).subscribe( ret => {
        this.a.login( ret );
        this.toHome();
    }, error => {
      console.log( 'Login: ' + error.message );
      this.msgs = [ {severity: 'error', summary: 'Erro', detail: 'Usuário e/ou senha incorreto(s)!'} ];
      this.erro = true;
    });
  }

  toHome() {
    this.r.navigateByUrl( "home" );
  }

}
