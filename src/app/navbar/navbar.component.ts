import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor( private a: AuthService,
               private r: Router ) { }

  ngOnInit() { }

  loggout() {
    if ( confirm('Deseja realmente sair do sistema?') ) {
      this.a.loggout();
      this.r.navigateByUrl( "login" );
    }
  }

}