import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private a: AuthService, 
               private h: HttpClient,
               private r: Router ) { }

  ngOnInit() {
    if ( !this.a.isValidSeasson() ) {
      console.log( 'Sessao invalida' )
      this.a.loggout();
      this.toLogin();
    }
  }

  toLogin() {
    this.r.navigateByUrl( "login" );
  }

}