import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Categoria } from 'src/app/entity/categoria';
import { Empresa } from 'src/app/entity/empresa';
import { environment } from 'src/environments/environment';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  private ROLE : string = "ROLE_ADMIN";
  private WS = `${environment.api}/empresa/`;
  private WSC= `${environment.api}/categoria/`;

  formdata;

  categorias: Categoria[] = [];
  e: Empresa = new Empresa();
  title: string;

  constructor( private a: AuthService,
               private c: ActivatedRoute,
               private h: HttpClient,
               private r: Router,
               @Inject(FormBuilder) fb: FormBuilder ) { 

    this.formdata = fb.group({
      nome: [Validators.required, Validators.minLength(4)],
      cnpj: [Validators.required, Validators.pattern("\d{2}\.\d{3}\.\d{3}/\d{4}-\d{2}")],
      cate: ['']
    });

    if ( !this.a.hasRole( this.ROLE ) ) {
      this.toHome();
    } else {
      this.loadCategorias();
      let id = this.c.snapshot.paramMap.get( "id" );
      if ( id != 'novo' ) {
        this.title = "Editar registro";
        this.loadRegister( id );
      } else {
        this.title = "Novo registro";
        this.e.login = this.a.read();
      } 
    }

  }

  ngOnInit() { }

  loadCategorias() {
    this.h.get<Categoria[]>( this.WSC ).subscribe( ret => {      
      this.categorias = ret;      
    }, error => {
      if ( error.status == 401 ) {
        this.a.loggout();
      }
      this.toHome();
    }, () => console.log('categorias done'));
  }

  loadRegister( id : string ) {
    this.h.get<Empresa>( this.WS + id + '/' ).subscribe( ret => {
      this.e = ret;
    }, error => {
      if ( error.status == 401 ) {
        this.a.loggout();
      }
      this.toHome();
    }, () => console.log('load done'));
  }

  save( data ) {
    console.log('Save');
    console.log( data.error );
    if ( data.error == undefined ) {
      alert( data.Validators.error );
    }
  }
 
  toHome() {
    this.r.navigateByUrl( "home" );
  }

}
