import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Empresa } from 'src/app/entity/empresa';
import { Categoria } from 'src/app/entity/categoria';
import { AuthService } from 'src/app/auth.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  private ROLE : string = "ROLE_ADMIN";
  private WS = `${environment.api}/empresa/`;
  private WSC= `${environment.api}/categoria/`;
  
  total: number;
  index: number;
  empresas: Empresa[];
  categorias: Categoria[];

  constructor( private a: AuthService,
               private h: HttpClient,
               private r: Router ) {
    this.loadCategorias();
  }

  ngOnInit() {
    if ( !this.a.hasRole( this.ROLE ) ) {
      this.toHome();
    } else {
      this.list();
    }
    
  }

  loadCategorias() {
    this.h.get<Categoria[]>( this.WSC ).subscribe( ret => {      
      this.categorias = ret;      
    }, error => {
      if ( error.status == 401 ) {
        this.a.loggout();
      }
      this.toHome();
    });
  }

  categoriaName( id: number ) : string {
    for ( let cat of this.categorias ) {
      if ( cat.id == id ) {
        return cat.descricao;
      }
    }
    return 'N/A';
  }

  getPos() : string {
    var p : string = this.index.toString();
    this.index++;
    return p;
  }

  list() {    
    this.h.get<Empresa[]>( this.WS ).subscribe( ret => {
        this.index = 1;
        this.empresas = ret;
        this.total = this.empresas.length;
    }, error => {
      if ( error.status == 401 ) {
        this.a.loggout();
      }
      this.toHome();
    });
  }

  toHome() {
    this.r.navigateByUrl( "home" );
  }

}
