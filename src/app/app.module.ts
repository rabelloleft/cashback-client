import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AuthInterceptorService } from './service/auth-interceptor.service';
import { UserRoleDirective } from './user-role.directive';

import { GrowlModule } from 'primeng/growl';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ListaComponent } from './empresa/lista/lista.component';
import { EmpresaComponent } from './empresa/empresa/empresa.component';

@NgModule({
  declarations: [
    AppComponent,
    UserRoleDirective,
    HomeComponent,
    LoginComponent,
    NavbarComponent,
    ListaComponent,
    EmpresaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    GrowlModule,

    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi : true }
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
