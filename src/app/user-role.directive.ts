import { Directive, OnInit, Input, ElementRef } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[userRole]'
})

export class UserRoleDirective implements OnInit {

  @Input() userRole: string = "";

  constructor( private e: ElementRef,
               private a: AuthService ) {
  }

  ngOnInit() {
    if ( !this.a.hasRole( this.userRole ) ) {
      this.e.nativeElement.remove();
    }
  }

}