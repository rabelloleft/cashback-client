export class LoginEntry{
    login : string
    senha : string
}

export class LoginReturn {
    id : number
    nome : string
    token : string
    papeis : string[]
}