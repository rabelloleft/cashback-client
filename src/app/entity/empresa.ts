import { LoginReturn } from './login';

export class Empresa {
    id : number;
    nome : string;
    cnpj : string;
    idcategoria : number;
    login : LoginReturn;
}